#1 Hello World
'''
print("Hello, World!")
'''

#2 DrawStar
def DrawStar(n, order):
	if order=="AS":
		i=n
		while i>0:
			print('*'*i)
			i=i-1
	elif order=="DES":
		i=0
		while i<n+1:
			print('*'*i)
			i=i+1


'''
DrawStar(5,"AS")
DrawStar(7, "DES")
'''

#3 reverse Str
def ReverseStr(str):
	print(str)
	i = len(str)
	revStr = ''
	while i>0:
		i=i-1
		revStr=revStr+str[i]
		#print(str[i], end='')
	print(revStr)

'''
ReverseStr("abcdefg")
'''

#4 Bubble Sort
def Swap(a, b):
	temp=a
	a=b
	b=temp


def BubbleSort(list, order):
	print(list)
	i=len(list)
	while i>0:
		for k in range(0, i-1):
			if (order=="AS" and list[k]<list[k+1]) or (order=="DES" and list[k]>list[k+1]):
				temp = list[k]
				list[k]=list[k+1]
				list[k+1]=temp
		i=i-1
	print(list)

		
BubbleSort([9, 4, 6, 2, 5, 7, 10, 8, 1, 3], "AS")
BubbleSort([9, 4, 6, 2, 5, 7, 10, 8, 1, 3], "DES")